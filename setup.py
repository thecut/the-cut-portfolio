from setuptools import setup, find_packages
from thecut.portfolio import __VERSION__


setup(
    name='thecut-portfolio',
    author='The Cut',
    author_email='development@thecut.net.au',
    url='http://projects.thecut.net.au/projects/thecut-portfolio',
    namespace_packages=['thecut'],
    version=__VERSION__,
    packages=find_packages(),
    include_package_data=True,
)
