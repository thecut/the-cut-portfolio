# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models
from django.conf import settings

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Deleting field 'Project.feature_image'
        db.delete_column('portfolio_project', 'feature_image')

        # Deleting field 'Project.summary'
        db.delete_column('portfolio_project', 'summary')

        # Adding field 'Project.is_featured'
        db.add_column('portfolio_project', 'is_featured', self.gf('django.db.models.fields.BooleanField')(default=False), keep_default=False)

        # Adding field 'Project.expire_at'
        db.add_column('portfolio_project', 'expire_at', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True), keep_default=False)

        # Adding field 'Project.publish_by'
        db.add_column('portfolio_project', 'publish_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='project_publish_by_user', null=True, to=orm[settings.AUTH_USER_MODEL]), keep_default=False)

        # Adding field 'Project.order'
        db.add_column('portfolio_project', 'order', self.gf('django.db.models.fields.PositiveIntegerField')(default=0), keep_default=False)

        # Adding unique constraint on 'Project', fields ['client', 'slug']
        db.create_unique('portfolio_project', ['client_id', 'slug'])

        # Deleting field 'Client.summary'
        db.delete_column('portfolio_client', 'summary')

        # Adding field 'Client.is_featured'
        db.add_column('portfolio_client', 'is_featured', self.gf('django.db.models.fields.BooleanField')(default=False), keep_default=False)

        # Adding field 'Client.expire_at'
        db.add_column('portfolio_client', 'expire_at', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True), keep_default=False)

        # Adding field 'Client.publish_by'
        db.add_column('portfolio_client', 'publish_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='client_publish_by_user', null=True, to=orm[settings.AUTH_USER_MODEL]), keep_default=False)

        # Adding field 'Client.template'
        db.add_column('portfolio_client', 'template', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True), keep_default=False)

        # Adding field 'Client.order'
        db.add_column('portfolio_client', 'order', self.gf('django.db.models.fields.PositiveIntegerField')(default=0), keep_default=False)


    def backwards(self, orm):
        
        # Removing unique constraint on 'Project', fields ['client', 'slug']
        db.delete_unique('portfolio_project', ['client_id', 'slug'])

        # Adding field 'Project.feature_image'
        db.add_column('portfolio_project', 'feature_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True), keep_default=False)

        # Adding field 'Project.summary'
        db.add_column('portfolio_project', 'summary', self.gf('django.db.models.fields.TextField')(null=True, blank=True), keep_default=False)

        # Deleting field 'Project.is_featured'
        db.delete_column('portfolio_project', 'is_featured')

        # Deleting field 'Project.expire_at'
        db.delete_column('portfolio_project', 'expire_at')

        # Deleting field 'Project.publish_by'
        db.delete_column('portfolio_project', 'publish_by_id')

        # Deleting field 'Project.order'
        db.delete_column('portfolio_project', 'order')

        # Adding field 'Client.summary'
        db.add_column('portfolio_client', 'summary', self.gf('django.db.models.fields.TextField')(null=True, blank=True), keep_default=False)

        # Deleting field 'Client.is_featured'
        db.delete_column('portfolio_client', 'is_featured')

        # Deleting field 'Client.expire_at'
        db.delete_column('portfolio_client', 'expire_at')

        # Deleting field 'Client.publish_by'
        db.delete_column('portfolio_client', 'publish_by_id')

        # Deleting field 'Client.template'
        db.delete_column('portfolio_client', 'template')

        # Deleting field 'Client.order'
        db.delete_column('portfolio_client', 'order')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        settings.AUTH_USER_MODEL: {
            'Meta': {'object_name': settings.AUTH_USER_MODEL.split('.')[-1]},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'portfolio.client': {
            'Meta': {'ordering': "['order', 'title']", 'unique_together': "(['site', 'slug'],)", 'object_name': 'Client'},
            'content': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'client_created_by_user'", 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)}),
            'expire_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'headline': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_indexable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'publish_at': ('django.db.models.fields.DateTimeField', [], {}),
            'publish_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'client_publish_by_user'", 'null': 'True', 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sites.Site']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'db_index': 'True'}),
            'tags': ('tagging.fields.TagField', [], {'null': 'True'}),
            'template': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'updated_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'client_updated_by_user'", 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)})
        },
        'portfolio.project': {
            'Meta': {'ordering': "['order', 'client', 'title']", 'unique_together': "(['client', 'slug'],)", 'object_name': 'Project'},
            'client': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Client']"}),
            'content': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'project_created_by_user'", 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)}),
            'expire_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'headline': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_indexable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'publish_at': ('django.db.models.fields.DateTimeField', [], {}),
            'publish_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'project_publish_by_user'", 'null': 'True', 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'db_index': 'True'}),
            'tags': ('tagging.fields.TagField', [], {'null': 'True'}),
            'template': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'updated_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'project_updated_by_user'", 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)})
        },
        'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['portfolio']
