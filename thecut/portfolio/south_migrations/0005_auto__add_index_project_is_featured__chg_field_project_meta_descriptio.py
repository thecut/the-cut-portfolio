# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models
from django.conf import settings


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding index on 'Project', fields ['is_featured']
        db.create_index('portfolio_project', ['is_featured'])


        # Changing field 'Project.meta_description'
        db.alter_column('portfolio_project', 'meta_description', self.gf('django.db.models.fields.CharField')(max_length=200))

        # Changing field 'Project.headline'
        db.alter_column('portfolio_project', 'headline', self.gf('django.db.models.fields.CharField')(max_length=200))

        # Changing field 'Project.content'
        db.alter_column('portfolio_project', 'content', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Project.template'
        db.alter_column('portfolio_project', 'template', self.gf('django.db.models.fields.CharField')(max_length=100))
        # Adding index on 'Project', fields ['is_enabled']
        db.create_index('portfolio_project', ['is_enabled'])


        # Changing field 'Project.tags'
        db.alter_column('portfolio_project', 'tags', self.gf('tagging.fields.TagField')())
        # Adding index on 'Project', fields ['is_indexable']
        db.create_index('portfolio_project', ['is_indexable'])

        # Adding index on 'Project', fields ['publish_at']
        db.create_index('portfolio_project', ['publish_at'])


        # Changing field 'Project.featured_content'
        db.alter_column('portfolio_project', 'featured_content', self.gf('django.db.models.fields.TextField')())
        # Adding index on 'Project', fields ['expire_at']
        db.create_index('portfolio_project', ['expire_at'])

        # Adding index on 'Client', fields ['is_featured']
        db.create_index('portfolio_client', ['is_featured'])


        # Changing field 'Client.meta_description'
        db.alter_column('portfolio_client', 'meta_description', self.gf('django.db.models.fields.CharField')(max_length=200))

        # Changing field 'Client.headline'
        db.alter_column('portfolio_client', 'headline', self.gf('django.db.models.fields.CharField')(max_length=200))

        # Changing field 'Client.content'
        db.alter_column('portfolio_client', 'content', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Client.template'
        db.alter_column('portfolio_client', 'template', self.gf('django.db.models.fields.CharField')(max_length=100))
        # Adding index on 'Client', fields ['is_enabled']
        db.create_index('portfolio_client', ['is_enabled'])


        # Changing field 'Client.tags'
        db.alter_column('portfolio_client', 'tags', self.gf('tagging.fields.TagField')())
        # Adding index on 'Client', fields ['is_indexable']
        db.create_index('portfolio_client', ['is_indexable'])

        # Adding index on 'Client', fields ['publish_at']
        db.create_index('portfolio_client', ['publish_at'])


        # Changing field 'Client.featured_content'
        db.alter_column('portfolio_client', 'featured_content', self.gf('django.db.models.fields.TextField')())
        # Adding index on 'Client', fields ['expire_at']
        db.create_index('portfolio_client', ['expire_at'])


    def backwards(self, orm):
        # Removing index on 'Client', fields ['expire_at']
        db.delete_index('portfolio_client', ['expire_at'])

        # Removing index on 'Client', fields ['publish_at']
        db.delete_index('portfolio_client', ['publish_at'])

        # Removing index on 'Client', fields ['is_indexable']
        db.delete_index('portfolio_client', ['is_indexable'])

        # Removing index on 'Client', fields ['is_enabled']
        db.delete_index('portfolio_client', ['is_enabled'])

        # Removing index on 'Client', fields ['is_featured']
        db.delete_index('portfolio_client', ['is_featured'])

        # Removing index on 'Project', fields ['expire_at']
        db.delete_index('portfolio_project', ['expire_at'])

        # Removing index on 'Project', fields ['publish_at']
        db.delete_index('portfolio_project', ['publish_at'])

        # Removing index on 'Project', fields ['is_indexable']
        db.delete_index('portfolio_project', ['is_indexable'])

        # Removing index on 'Project', fields ['is_enabled']
        db.delete_index('portfolio_project', ['is_enabled'])

        # Removing index on 'Project', fields ['is_featured']
        db.delete_index('portfolio_project', ['is_featured'])


        # Changing field 'Project.meta_description'
        db.alter_column('portfolio_project', 'meta_description', self.gf('django.db.models.fields.CharField')(max_length=200, null=True))

        # Changing field 'Project.headline'
        db.alter_column('portfolio_project', 'headline', self.gf('django.db.models.fields.CharField')(max_length=200, null=True))

        # Changing field 'Project.content'
        db.alter_column('portfolio_project', 'content', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Project.template'
        db.alter_column('portfolio_project', 'template', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Project.tags'
        db.alter_column('portfolio_project', 'tags', self.gf('tagging.fields.TagField')(null=True))

        # Changing field 'Project.featured_content'
        db.alter_column('portfolio_project', 'featured_content', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Client.meta_description'
        db.alter_column('portfolio_client', 'meta_description', self.gf('django.db.models.fields.CharField')(max_length=200, null=True))

        # Changing field 'Client.headline'
        db.alter_column('portfolio_client', 'headline', self.gf('django.db.models.fields.CharField')(max_length=200, null=True))

        # Changing field 'Client.content'
        db.alter_column('portfolio_client', 'content', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Client.template'
        db.alter_column('portfolio_client', 'template', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Client.tags'
        db.alter_column('portfolio_client', 'tags', self.gf('tagging.fields.TagField')(null=True))

        # Changing field 'Client.featured_content'
        db.alter_column('portfolio_client', 'featured_content', self.gf('django.db.models.fields.TextField')(null=True))

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        settings.AUTH_USER_MODEL: {
            'Meta': {'object_name': settings.AUTH_USER_MODEL.split('.')[-1]},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'ctas.calltoaction': {
            'Meta': {'ordering': "(u'order',)", 'object_name': 'CallToAction'},
            'content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)}),
            'expire_at': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_featured': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'publish_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'db_index': 'True'}),
            'publish_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)}),
            'source_content_type': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'calltoaction_source_set'", 'null': 'True', 'to': "orm['contenttypes.ContentType']"}),
            'source_object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'updated_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)})
        },
        'media.attachedmediaitem': {
            'Meta': {'ordering': "(u'order',)", 'object_name': 'AttachedMediaItem'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'parent_content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'attachedmediaitem_parent_set'", 'to': "orm['contenttypes.ContentType']"}),
            'parent_object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'})
        },
        'portfolio.client': {
            'Meta': {'ordering': "(u'order',)", 'unique_together': "((u'site', u'slug'),)", 'object_name': 'Client'},
            'content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)}),
            'expire_at': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'featured_content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'headline': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_featured': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'is_indexable': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'publish_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'db_index': 'True'}),
            'publish_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sites.Site']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'tags': ('tagging.fields.TagField', [], {'default': "u''"}),
            'template': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '100', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'updated_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)})
        },
        'portfolio.project': {
            'Meta': {'ordering': "(u'order',)", 'unique_together': "((u'client', u'slug'),)", 'object_name': 'Project'},
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'projects'", 'to': "orm['portfolio.Client']"}),
            'content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)}),
            'expire_at': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'featured_content': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'headline': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_featured': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'is_indexable': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '200', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'publish_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'db_index': 'True'}),
            'publish_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'tags': ('tagging.fields.TagField', [], {'default': "u''"}),
            'template': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '100', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'updated_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': "orm['{0}']".format(settings.AUTH_USER_MODEL)})
        },
        'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['portfolio']