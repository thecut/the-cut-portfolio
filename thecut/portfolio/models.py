# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from thecut.ordering.models import OrderMixin
from thecut.publishing.models import SiteContentWithSlug
from thecut.publishing.utils import generate_unique_slug


class AbstractClient(OrderMixin, SiteContentWithSlug):

    link = models.URLField(blank=True, default='')
    logo = models.ImageField(
        upload_to='uploads/portfolio/clients/logos',
        blank=True, null=True)

    class Meta(OrderMixin.Meta, SiteContentWithSlug.Meta):
        abstract = True


class Client(AbstractClient):

    class Meta(AbstractClient.Meta):
        pass

    def get_absolute_url(self):
        return reverse('portfolio:client_detail', kwargs={'slug': self.slug})


class AbstractProject(OrderMixin, SiteContentWithSlug):

    link = models.URLField(blank=True, default='')

    class Meta(OrderMixin.Meta, SiteContentWithSlug.Meta):
        abstract = True


class Project(AbstractProject):

    """A client project or case-study."""

    client = models.ForeignKey('portfolio.Client', related_name='projects')

    class Meta(AbstractProject.Meta):
        pass

    def get_absolute_url(self):
        return reverse('portfolio:project_detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        try:
            self.client
        except ObjectDoesNotExist:
            pass
        else:
            if not self.slug:
                queryset = self.__class__.objects.filter(site=self.site)
                slug = '{0}-{1}'.format(self.client, self)
                self.slug = generate_unique_slug(slug, queryset)
        return super(Project, self).save(*args, **kwargs)
