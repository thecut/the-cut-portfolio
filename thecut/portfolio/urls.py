# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from .feeds import LatestProjectFeed
from .views import client, project
from django.conf.urls import include, url

urls = [

    url(r'^clients/$',
        client.ListView.as_view(), name='client_list'),
    url(r'^clients/(?P<page>\d+)$',
        client.ListView.as_view(), name='paginated_client_list'),
    url(r'^clients/(?P<slug>[\w-]+)/$',
        client.DetailView.as_view(), name='client_detail'),

    # url(r'^clients/(?P<client_slug>[\w-]+)/projects/$',
    #     project.ListView.as_view(), name='client_project_list'),
    # url(r'^clients/(?P<client_slug>[\w-]+)/projects/(?P<page>\d+)$',
    #     project.ListView.as_view(), name='paginated_client_project_list'),

    url(r'^projects/$',
        project.ListView.as_view(), name='project_list'),
    url(r'^projects/(?P<page>\d+)$',
        project.ListView.as_view(), name='paginated_project_list'),
    url(r'^projects/(?P<slug>[\w-]+)/$',
        project.DetailView.as_view(), name='project_detail'),

    url(r'^projects/latest\.xml$', LatestProjectFeed(), name='project_feed'),

]

urlpatterns = [
    url(r'^', include(urls, namespace='portfolio')),
]
