# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.conf import settings


PAGINATE_BY = getattr(settings, 'PORTFOLIO_PAGINATE_BY', 10)

CLIENT_PAGINATE_BY = getattr(settings, 'PORTFOLIO_CLIENT_PAGINATE_BY',
                             PAGINATE_BY)

PROJECT_PAGINATE_BY = getattr(settings, 'PORTFOLIO_PROJECT_PAGINATE_BY',
                              PAGINATE_BY)
