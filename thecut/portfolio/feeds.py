# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from .models import Project
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
from django.contrib.syndication.views import Feed
from django.utils.feedgenerator import Atom1Feed


class LatestProjectFeed(Feed):

    feed_type = Atom1Feed
    subtitle = 'Latest projects.'

    def link(self):
        return reverse('portfolio:project_list')

    def title(self):
        site = Site.objects.get_current()
        return '%s - Projects' % (site.name)

    def items(self):
        return Project.objects.current_site().active().order_by(
            '-publish_at')[:10]

    def item_title(self, item):
        return '{}'.format(item)

    def item_description(self, item):
        return item.content

    def item_pubdate(self, item):
        return item.publish_at
