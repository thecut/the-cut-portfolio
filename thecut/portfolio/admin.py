# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from .models import Client, Project
from django.contrib import admin
from thecut.ordering.admin import ReorderMixin
from thecut.authorship.admin import AuthorshipMixin


class ClientAdmin(ReorderMixin, AuthorshipMixin, admin.ModelAdmin):

    fieldsets = (
        (None, {'fields': ('title', 'headline', 'link', 'featured_content',
                           'content', 'logo', 'meta_description', 'tags')}),
        ('Publishing', {'fields': ('site', 'slug',
                                   ('publish_at', 'is_enabled'), 'expire_at',
                                   'publish_by', 'template', 'is_featured',
                                   'is_indexable', 'order',
                                   ('created_at', 'created_by'),
                                   ('updated_at', 'updated_by')),
                        'classes': ('collapse',)}),
    )
    list_display = ('title', 'publish_at', 'is_enabled', 'is_featured',
                    'is_indexable', 'site')
    list_filter = ('publish_at', 'is_enabled', 'is_featured', 'is_indexable')
    prepopulated_fields = {'slug': ('title',)}
    readonly_fields = ('created_at', 'created_by', 'updated_at', 'updated_by')
    search_fields = ('title',)

admin.site.register(Client, ClientAdmin)


class ProjectAdmin(ReorderMixin, AuthorshipMixin, admin.ModelAdmin):

    fieldsets = (
        (None, {'fields': ('client', 'title', 'headline', 'link',
                           'featured_content', 'content', 'meta_description',
                           'tags')}),
        ('Publishing', {'fields': ('site', 'slug',
                                   ('publish_at', 'is_enabled'), 'expire_at',
                                   'publish_by', 'template', 'is_featured',
                                   'is_indexable', 'order',
                                   ('created_at', 'created_by'),
                                   ('updated_at', 'updated_by')),
                        'classes': ('collapse',)}),
    )
    list_display = ('title', 'publish_at', 'is_enabled', 'is_featured',
                    'is_indexable', 'site')
    list_filter = ('publish_at', 'is_enabled', 'is_featured', 'is_indexable')
    prepopulated_fields = {'slug': ('title',)}
    readonly_fields = ('created_at', 'created_by', 'updated_at', 'updated_by')
    search_fields = ('title',)

admin.site.register(Project, ProjectAdmin)
