# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from distutils.version import StrictVersion
from django import get_version
from django.shortcuts import get_object_or_404, redirect
from django.views import generic
from thecut.portfolio import settings
from thecut.portfolio.models import Client, Project



class DetailView(generic.DetailView):

    context_object_name = 'project'
    model = Project
    template_name_field = 'template'

    def get_queryset(self, *args, **kwargs):
        queryset = super(DetailView, self).get_queryset(*args, **kwargs)
        return queryset.current_site().active()


class ListView(generic.ListView):

    context_object_name = 'project_list'
    model = Project
    paginate_by = settings.PROJECT_PAGINATE_BY

    def get(self, *args, **kwargs):
        page = self.kwargs.get('page', None)
        if page is not None and int(page) < 2:
            client = self.get_client()
            if client:
                return redirect('portfolio:client_project_list',
                                slug=client.slug, permanent=True)
            else:
                return redirect('portfolio:project_list', permanent=True)
        return super(ListView, self).get(*args, **kwargs)

    def get_client(self):
        if not hasattr(self, '_client'):
            slug = self.kwargs.get('client_slug', None)
            if slug is not None:
                client = get_object_or_404(
                    Client.objects.current_site().active(), slug=slug)
            else:
                client = None
            self._client = client
        return self._client

    def get_context_data(self, *args, **kwargs):
        context_data = super(ListView, self).get_context_data(*args, **kwargs)
        context_data.setdefault('client', self.get_client())
        return context_data

    def get_queryset(self, *args, **kwargs):
        queryset = super(ListView, self).get_queryset(*args, **kwargs)
        client = self.get_client()
        if client:
            queryset = queryset.filter(client=client)
        return queryset.current_site().active()
