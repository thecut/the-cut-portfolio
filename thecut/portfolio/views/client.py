# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from distutils.version import StrictVersion
from django import get_version
from django.shortcuts import redirect
from django.views import generic
from thecut.portfolio import settings
from thecut.portfolio.models import Client


class DetailView(generic.DetailView):

    context_object_name = 'client'
    model = Client
    template_name_field = 'template'

    def get_queryset(self, *args, **kwargs):
        queryset = super(DetailView, self).get_queryset(*args, **kwargs)
        return queryset.current_site().active()


class ListView(generic.ListView):

    context_object_name = 'client_list'
    model = Client
    paginate_by = settings.CLIENT_PAGINATE_BY

    def get(self, *args, **kwargs):
        page = self.kwargs.get('page', None)
        if page is not None and int(page) < 2:
            return redirect('portfolio:client_list', permanent=True)
        return super(ListView, self).get(*args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        queryset = super(ListView, self).get_queryset(*args, **kwargs)
        return queryset.current_site().active()
