# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals


default_app_config = 'thecut.portfolio.apps.AppConfig'

__VERSION__ = '0.7.2'
