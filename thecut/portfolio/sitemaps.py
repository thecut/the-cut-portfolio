# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import settings
from .models import Client, Project
from django.contrib.sitemaps import Sitemap
from django.core.paginator import Paginator
from django.core.urlresolvers import reverse


class ClientSitemap(Sitemap):

    def items(self):
        return Client.objects.current_site().indexable()

    def lastmod(self, obj):
        return obj.updated_at


class ClientListSitemap(Sitemap):

    def items(self):
        objects = Client.objects.current_site().indexable()
        page_size = settings.CLIENT_PAGINATE_BY
        return Paginator(objects, page_size).page_range if page_size else [1]

    def location(self, page):
        if page == 1:
            return reverse('portfolio:client_list')
        else:
            return reverse('portfolio:paginated_client_list',
                           kwargs={'page': page})


class ProjectSitemap(Sitemap):

    def items(self):
        return Project.objects.current_site().indexable()

    def lastmod(self, obj):
        return obj.updated_at


class ProjectListSitemap(Sitemap):

    def items(self):
        objects = Project.objects.current_site().indexable()
        page_size = settings.PROJECT_PAGINATE_BY
        return Paginator(objects, page_size).page_range if page_size else [1]

    def location(self, page):
        if page == 1:
            return reverse('portfolio:project_list')
        else:
            return reverse('portfolio:paginated_project_list',
                           kwargs={'page': page})


class ClientProjectListSitemap(Sitemap):

    def items(self):
        clients = Client.objects.current_site().indexable()
        items = []
        for client in clients:
            objects = client.projects.current_site().indexable()
            page_size = settings.PROJECT_PAGINATE_BY
            page_range = Paginator(objects, page_size).page_range \
                if page_size else [1]
            items += [(client.slug, page) for page in page_range]
        return items

    def location(self, opts):
        slug, page = opts
        if page == 1:
            return reverse('portfolio:client_project_list',
                           kwargs={'client_slug': slug})
        else:
            return reverse('portfolio:paginated_client_project_list',
                           kwargs={'client_slug': slug, 'page': page})


sitemaps = {'portfolio_client': ClientSitemap,
            'portfolio_clientlist': ClientListSitemap,
            'portfolio_project': ProjectSitemap,
            'portfolio_projectlist': ProjectListSitemap,
            'portfolio_clientprojectlist': ClientProjectListSitemap}
